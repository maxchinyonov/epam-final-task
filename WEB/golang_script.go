package main

import (
        "database/sql"
        "fmt"
        _ "github.com/lib/pq"
        "os"
)

type article struct {
        id    int
        value string
}

func main() {
        file, err := os.Create("/usr/share/nginx/html_ui/index.html")
        if err != nil {
                fmt.Println(err)
                os.Exit(1)
        }
        defer file.Close()

        fmt.Fprintln(file,
                "<!DOCTYPE html>\n",
                "<HTML>\n",
                "<Body>")

        connStr := "user=viewer dbname=postgres sslmode=disable host=intranet-2.database.final.task"
        db, err := sql.Open("postgres", connStr)

        if err != nil {
                panic(err)
        }
        defer db.Close()

        strings := []string{"select * from author", "select * from magazines", "select * from article_types"}
        for _, s := range strings {
                rows, err := db.Query(s)
                if err != nil {
                        panic(err)
                }
                defer rows.Close()
                articles := []article{}

                for rows.Next() {
                        a := article{}
                        err := rows.Scan(&a.id, &a.value)
                        if err != nil {
                                fmt.Println(err)
                                continue
                        }
                        articles = append(articles, a)
                }

                switch {
                case s == "select * from author":
                        fmt.Fprintln(file, "<h1> Authors </h1>")
                case s == "select * from magazines":
                        fmt.Fprintln(file, "<h1> Magazines </h1>")
                case s == "select * from article_types":
                        fmt.Fprintln(file, "<h1> Article Types </h1>")
                }

                for _, a := range articles {

                        fmt.Fprintln(file, "<div>", a.id, a.value, "</div>")
                }
                fmt.Fprintln(file, "<p>", " ", "</p>")
        }
        fmt.Fprintln(file,
                "</Body>\n",
                "</HTML>")
}
