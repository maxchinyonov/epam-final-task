#!/bin/bash


# Colours
red='\033[0;31m'
green='\033[0;32m'
clear='\033[0m'

# Script

files='/local/files/'
backups='/local/backups/'

# fileCount=$(ls -l ${files} | grep 'articles_' | cut -f 9 -d ' ' | wc -w)
fileCount=$(find ${files} -type f | grep 'articles_' | wc -l)
fileNumber=$(ls -l ${files} | tail -n +2 | awk '{print $9}' | awk -F '_' '{print $2}' | sort -n | tail -1)
archiveCount=$((`ls -l ${backups} | tail -n +2 | awk '{print $9}' | awk -F '_' '{print $1}' | sort -n | tail -1` +1))

if (( $fileCount < 3 ))
   then
     fileNumber=$(($fileNumber + 1))

     psql -d postgres --command='select * from articles;' > ${files}articles_$fileNumber
     psql -d postgres --command='select * from magazines;' >> ${files}articles_$fileNumber
     psql -d postgres --command='select * from article_types;' >> ${files}articles_$fileNumber
     psql -d postgres --command='select * from author;' >> ${files}articles_$fileNumber

else
     cd /local/files && \
     echo -e "${red}Archive \"${archiveCount}_archive.tar.gz\" created include files:${clear}" && \
     tar -zcvf ${archiveCount}_archive.tar.gz articles_* && \
     mv ${files}${archiveCount}_archive.tar.gz ${backups} && \
     echo -e "${green}Files archived, compressed and moved to directory ${backups}${clear}"

     fileNumber=$(($fileNumber + 1))

     rm -rf ${files}articles_*
     psql -d postgres --command='select * from articles;' > ${files}articles_$fileNumber
     psql -d postgres --command='select * from magazines;' >> ${files}articles_$fileNumber
     psql -d postgres --command='select * from article_types;' >> ${files}articles_$fileNumber
     psql -d postgres --command='select * from author;' >> ${files}articles_$fileNumber

fi
