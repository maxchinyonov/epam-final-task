#!/bin/bash

# Import config file
source /etc/sysconfig/script2.sh

# Script

echo $$ > /var/run/script2.pid

for ((;;))
do
number_of_files=$(find /local/backups/ -type f | wc -l)
disk_usage=$(du -b /local/backups | awk '{print $1}')
if (( $number_of_files > $files ))
then
    mail -s "Number of files in /local/backups directory is more than $files. Number of files is $number_of_files" root < . > /dev/null 2>&1
fi

####################################################

if (( $disk_usage > $bytes ))
then
    mail -s "Total size of /local/backups directory is more than $bytes bytes. Total $disk_usage bytes" root < . > /dev/null 2>&1
fi

sleep 300
done
